LLOC     = 41
IF_TRUNK = 'eth4'


conf_ifaces="""
/interface vlan add vlan-id={lloc}{xarxa_id} interface={ifname} name=vnet{xarxa_id}
/ip address add interface=vnet{xarxa_id} address=10.1{lloc}.{xarxa_id}.1/24
/ip pool add ranges=10.1{lloc}.{xarxa_id}.100-10.1{lloc}.{xarxa_id}.199 name=pool{xarxa_id} 
/ip dhcp-server network add dns-server=8.8.8.8 gateway=10.1{lloc}.{xarxa_id}.1 \\
    netmask=24 domain=meloinvento.com address=10.1{lloc}.{xarxa_id}.0/24
/ip dhcp-server add address-pool=pool{xarxa_id} interface=vnet{xarxa_id} disabled=no
"""
route = "/ip route add dst-address=10.1{lloc}.0.0/17 gateway=192.168.3.2{lloc}"

for id in range(2,7):
	print(conf_ifaces.format(lloc=LLOC,xarxa_id=id,ifname=IF_TRUNK))
	
for lloc in range(1,36):
	print(route.format(lloc=str(lloc).zfill(2)))
	
# INTERNET CONNECTION VIA ETH3
conf_inet_out="""
/ip address add interface=eth3 address=192.168.3.2{}/16
/ip route add dst-address=0.0.0.0/0 gateway=192.168.0.1
/ip firewall nat
add action=masquerade chain=srcnat out-interface=eth3
""".format(LLOC)

#QOS
conf_qos = """
/ip firewall mangle add chain=prerouting in-interface=vnet2 action=mark-packet new-packet-mark=fromvnet2
/queue tree
add burst-limit=10M burst-threshold=100k burst-time=30s limit-at=200k \
    max-limit=200k name=queue1 packet-mark=no-mark parent=eth4 queue=\
    pcq-download-default
"""

# MANGLE EJEMPLO
"""
/ip firewall mangle
add action=mark-connection chain=prerouting connection-state=new \
    new-connection-mark=new77 src-address=192.168.88.77
add action=mark-packet chain=prerouting connection-mark=new77 \
    new-packet-mark=host77

"""

	

	
