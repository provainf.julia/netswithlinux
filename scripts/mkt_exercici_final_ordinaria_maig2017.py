#EJEMPLO PARA EL PUESTO29

/interface vlan add vlan-id=729 interface=eth3 name=deptA
/interface vlan add vlan-id=829 interface=eth3 name=deptB
/interface vlan add vlan-id=929 interface=eth3 name=servers
/interface vlan add vlan-id=1000 interface=eth1 name=escola
/interface vlan add vlan-id=1005 interface=eth1 name=isp

/ip address add interface=deptA address=10.27.129.1/24
/ip address add interface=deptB address=10.28.129.1/24
/ip address add interface=servers address=10.29.129.1/24
/ip address add interface=escola address=192.168.3.229/16
/ip address add interface=isp address=10.30.0.129/24

#
